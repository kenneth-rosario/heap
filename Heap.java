package Heap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Stack;

/**
 * 
 * @author kenneth-rosario
 *
 * @param <E> Type to store in heap
 */

public class Heap<E> implements Iterable<E>{
	
	private ArrayList<E> internalTree = new ArrayList<E>();
	private Comparator<E> cmp;
	
	private static class DefaultComparator<E> implements Comparator<E>{

		@Override
		public int compare(E o1, E o2) {
			// TODO Auto-generated method stub
			if(o1 instanceof Comparable && o2 instanceof Comparable) {
				return ((Comparable)o1).compareTo(o2);
			}
			throw new ClassCastException("Items are not comparable");
		}
		
	}
	
	public Heap() {
		this.cmp = new DefaultComparator<E>();
	}
	
	public Heap(Comparator<E> cmp) {
		this.cmp = cmp;
	}
	
	private int left(int i) {
		return 2*i + 1;
	}
	
	private boolean hasLeft(int i) {
		return 2*i + 1 < internalTree.size();
	}
	
	private int right(int i) {
		return 2*i + 2;
	}
	
	private boolean hasRight(int i) {
		return 2*i + 2 < internalTree.size();
	}
	
	private int parent(int i) {
		return (i-1)/2;
	}
	
	private void swap(int i, int j) {
		E temp = internalTree.get(i);
		internalTree.set(i, internalTree.get(j));
		internalTree.set(j, temp);
	}
	
	private void upHeap(int i) {
		while(i > 0) {
			E parent = internalTree.get(parent(i));
			if( cmp.compare(internalTree.get(i), parent) < 0 ) {
				swap(i, parent(i));
				i = parent(i);
			}else {
				break;
			}
		}
	}
	
	private void downHeap(int i) {
		while( hasLeft(i) ) {
			int smallerChildIndex = left(i);
			if(hasRight(i)) {
				E rightChild = internalTree.get(right(i));
				if(cmp.compare(rightChild, internalTree.get(smallerChildIndex)) < 0) {
					smallerChildIndex = right(i);
				}
			}
			if(cmp.compare(internalTree.get(i), internalTree.get(smallerChildIndex)) < 0) {
				break;
			}
			swap(i, smallerChildIndex);
			i = smallerChildIndex;
		}
	}
	
	public void insert(E e) {
		internalTree.add(e);
		upHeap(internalTree.size() - 1);
	}
	
	public E removeMin() {
		E temp = internalTree.get(0);
		swap(0, internalTree.size()-1);
		internalTree.remove(internalTree.size() - 1);
		downHeap(0);
		return temp;
	}
	
	public E min() {
		return internalTree.get(0);
	}


	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return internalTree.iterator();
	}
	
	
	public Iterable<E> preorder(){
		return new PreOrderIterable<E>();
	}
	
	
	private class PreOrder<E> implements Iterator<E>{
		private Stack<Integer> s = new Stack<>();
		
		public PreOrder() {
			if(!internalTree.isEmpty()) {
				s.push(0);
			}
		}
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return !s.isEmpty();
		}

		@Override
		public E next() {
			// TODO Auto-generated method stub
			if(!hasNext()) {
				throw new NoSuchElementException("No more next");
			}
			
			Integer currentIndex = s.pop();
			if(hasRight(currentIndex)) {
				s.push(right(currentIndex));
			}if(hasLeft(currentIndex)) {
				s.push(left(currentIndex));
			}
			
			return (E) internalTree.get(currentIndex);
		}
		
	}
	
	private class PreOrderIterable<E> implements Iterable<E>{

		@Override
		public Iterator<E> iterator() {
			// TODO Auto-generated method stub
			return new PreOrder();
		}
		
	}
	
	
	
	
}