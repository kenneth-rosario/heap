package Heap;

import java.util.ArrayList;
import java.util.Iterator;

public class heapTester {

	public static void main(String[] args) {
		Heap<Integer> h = new Heap<Integer>();
		h.insert(2);
		h.insert(0);
		h.insert(100);
		h.insert(7);
		System.out.println(h.removeMin());
		h.insert(10);
		h.insert(40);
		h.insert(5);
		ArrayList<Integer> iter = new ArrayList<>();
		for(Integer i: h.preorder()) {
			System.out.println(i);
		}
	}
}
